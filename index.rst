.. role:: emoji-size

.. meta::
    :description: کتاب آنلاین آموزش فارسی کار با گیت‌لب
    :keywords: گیت‌لب, گیت لب, فارسی, آموزش

راهنمای کار با gitlab
=====================

.. toctree::
    :maxdepth: 8

    book/01
    book/02
    book/03
    book/04
    book/05
    book/06
    book/07
    book/08
    book/09
   
|

----------------------


.. toctree::
    :maxdepth: 8
        
    attachs/ssh-key

|

-------------------

.. note::
    کتاب در حال توسعه است!
